# formverif
A Discord bot for verifying users using a Google form.

# How to self-host
1. Clone this repository
2. Copy the example `config.json` to the location you choose for your configuration.
3. Set up a bot on the Discord developer portal with permissions `268504132`.
4. Set `dbinfo` to your chosen database and location
5. Set `token` to your bot token
6. Set `owner` to the id of _your_ Discord user (enable developer mode and right click → Copy ID)
7. Run `fv_bot.py -c <path/to/your/config.json>`
8. Follow the steps in [How to add the bot to your server](#how-to-add-the-bot-to-your-server)

# How to add the bot to your server
1. Ask the owner to allowlist your server.  
If you are the owner, dm the bot the command `allow <server_id>`. You can get the server ID by right-clicking on the server and choosing "Copy ID".
2. If you are the owner, go to the link you got from the developer portal. If you are not, ask the owner for the link. Add the bot to your server.
3. Add `webhook.gs` to your Google Form (see [Configuring the Google Apps Script](#configuring-the-google-apps-script)).
4. Add a Discord channel visible to the bot for transmitting Google Forms data.
5. Add a [webhook](https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks) to the channel.
6. Set `<your_webhook_url>` in the script you set up for your Google Form.
7. Do `formverif help config` in your server to learn how to use the config command.
8. Set `verifiedRole` to the name of the role that the bot should add to verified users.
9. Set `botChannel` to the channel (mention the channel, don't just type the name) attached to your webhook.

## Configuring the Google Apps Script
1. Edit your Google Form and click "Script Editor"  
![Script Editor in the three dots menu](https://gitlab.com/danielrparks/formverif/-/raw/main/imgs/form_click_script_editor.png)
2. (Optional) Click on "Untitled Project" and give your project a name.
3. Paste `webhook.gs` into the script editor. Modify the script if necessary so that it correctly extracts the Discord username from the form response. By default, it expects it to be the response to the first question. **Remember to save!**  
![Picture of code after pasting](https://gitlab.com/danielrparks/formverif/-/raw/main/imgs/apps_script_paste.png)
3. Click on "Triggers".  
![Click the "Triggers" icon on the left](https://gitlab.com/danielrparks/formverif/-/raw/main/imgs/apps_script_click_triggers.png)
4. Click on "Add Trigger" in the bottom-right corner.
5. Set the script to run onSubmit when the form is submitted.  
![Picture of correct trigger settings](https://gitlab.com/danielrparks/formverif/-/raw/main/imgs/apps_script_trigger_settings.png)
