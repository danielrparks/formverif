import json
import formverif
import sys

config_file = "config.json"
if len(sys.argv) == 3 and sys.argv[1] == "-c":
    config_file = sys.argv[2]

with open(config_file) as f:
    config = json.load(f)

formverif.run(config)
