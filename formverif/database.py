import sqlite3
import formverif.guildConfig as guildConfig
import os
from datetime import datetime, timezone

schema = """\
CREATE TABLE guilds(guildId INT, botChannel INT, verifiedRole INT);
CREATE TABLE verified(guildId INT, responseId TEXT, userId INT, banned INT);
CREATE TABLE allowlist(guildId INT);
"""

class database:
    def __init__(self, dbinfo):
        if dbinfo["type"] == "sqlite3":
            try:
                self.mtime = datetime.utcfromtimestamp(os.stat(dbinfo["name"]).st_mtime)
            except OSError:
                self.mtime = 0
            self.conn = sqlite3.connect(dbinfo["name"])
            self.conn.row_factory = sqlite3.Row
            cur = self.conn.cursor()
            cur.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='guilds';")
            if cur.fetchone() is None:
                cur.executescript(schema)
                self.conn.commit()
        else:
            raise ValueError("Database type not supported")
        self.guilds = self._guildDict()
        cur = self.conn.cursor()
        cur.execute("SELECT guildId FROM allowlist;")
        self.allowed = set((row["guildId"] for row in cur.fetchall()))

    def _guildDict(self):
        result = dict()
        cur = self.conn.cursor()
        for row in cur.execute("SELECT guildId, botChannel, verifiedRole FROM guilds;"):
            result[row["guildId"]] = guildConfig.guildConfig(row)
        return result

    def defaultConfig(self, guildId):
        g = guildConfig.defaultConfig()
        self.guilds[guildId] = g
        cur = self.conn.cursor()
        cur.execute("INSERT INTO guilds VALUES (?, ?, ?);", (guildId, g.botChannel, g.verifiedRole))
        self.conn.commit()

    def removeConfig(self, guildId):
        cur = self.conn.cursor()
        cur.execute("DELETE FROM guilds WHERE guildId=?;", (guildId,))
        cur.execute("DELETE FROM verified WHERE guildId=?;", (guildId,))
        self.conn.commit()

    def _config(self, guildId, key, val):
        cur = self.conn.cursor()
        cur.execute("UPDATE guilds SET {}=? WHERE guildId=?;".format(key), (val, guildId))
        self.conn.commit()
        setattr(self.guilds[guildId], key, val)

    def configBotChannel(self, guildId, val):
        self._config(guildId, "botChannel", val)
    def configVerifiedRole(self, guildId, val):
        self._config(guildId, "verifiedRole", val)

    def getRes(self, guildId, resId):
        cur = self.conn.cursor()
        cur.execute("SELECT userId, banned FROM verified WHERE guildId=? AND responseId=?;", (guildId, resId))
        result = cur.fetchone()
        return (result["userId"], result["banned"]) if result else (None, None)

    def isVerified(self, guildId, uId):
        cur = self.conn.cursor()
        cur.execute("SELECT responseId FROM verified WHERE guildId=? AND userId=?;", (guildId, uId))
        return cur.fetchone() is not None

    def verify(self, guildId, responseId, uId, isUpdate):
        cur = self.conn.cursor()
        if isUpdate:
            cur.execute("UPDATE verified SET userId=? WHERE guildId=? AND responseId=?;", (uId, guildId, responseId))
        else:
            cur.execute("INSERT INTO verified VALUES (?, ?, ?, ?);", (guildId, responseId, uId, 0))
        self.conn.commit()

    def ban(self, guildId, uId):
        btime = datetime.now(timezone.utc).timestamp()
        cur = self.conn.cursor()
        cur.execute("UPDATE verified SET banned=? WHERE guildId=? AND userId=?;", (btime, guildId, uId))
        self.conn.commit()

    def unban(self, guildId, uId):
        cur = self.conn.cursor()
        cur.execute("UPDATE verified SET banned=0 WHERE guildId=? AND userId=?;", (guildId, uId))
        self.conn.commit()

    def syncbans(self, guildId, gBans):
        cur = self.conn.cursor()
        cur.execute("SELECT userId FROM verified where guildId=? AND banned!=0;", (guildId,))
        dBans = set((row["userId"] for row in cur.fetchall()))
        for uId in gBans - dBans:
            self.ban(guildId, uId)
        for uId in dBans - gBans:
            self.unban(guildId, uId)

    def allowGuild(self, guildId):
        cur = self.conn.cursor()
        cur.execute("INSERT INTO allowlist VALUES (?);", (guildId,))
        self.conn.commit()
        self.allowed.add(guildId)
