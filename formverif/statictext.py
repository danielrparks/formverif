nosuchcommand = "No such command, try `help`"
error = "Error: {error}"
badargs = "Bad arguments for command, see help"
publichelp = {
        "help": "see `help help`",
        "config": "(no arguments): view configuration for this server\n<key value>: set configuration for this server",
        }
helpappend = "\ntry `help <command>` to get help for a specific command"
globalhelp = "Valid commands: help [command], config [<key> <value>]" + helpappend
checkemoji = "✅"
badchannel = "incorrect channel specified"
badrole = "incorrect role specified"
badconfig = "no such configuration option"
unknownerror = "An unknown error has occurred. Please try again later or complain to the devs."
notallowed = "This server is not allowed to use this bot. Please contact the owner."
