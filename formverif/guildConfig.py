def defaultConfig():
    guildRow = dict()
    guildRow["verifiedRole"] = 0
    guildRow["botChannel"] = 0
    return guildConfig(guildRow)

class guildConfig:
    def __init__(self, guildRow):
        self.verifiedRole = guildRow["verifiedRole"]
        self.botChannel = guildRow["botChannel"]


    def config(self, guild):
        result = ""
        vr = guild.get_role(self.verifiedRole)
        vr = vr.name if vr else "UNCONFIGURED"
        bc = guild.get_channel(self.botChannel)
        bc = bc.mention if bc else "UNCONFIGURED"
        result += "verifiedRole: " + vr + "\n"
        result += "botChannel: " + bc + "\n"
        return result
