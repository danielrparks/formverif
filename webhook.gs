var POST_URL = "<your_webhook_url>";
var validResponse = new RegExp(/^[^#]+#\d{4}$/)

function onSubmit() {
  var lock = LockService.getUserLock();
  lock.waitLock(30000);
  var props = PropertiesService.getUserProperties();
  var cachedDate = new Date(props.getProperty("date"));
  Logger.log("Cached date: " + cachedDate.toString());
  var form = FormApp.getActiveForm();
  props.setProperty("date", new Date().toString());
  var newResponses = form.getResponses(cachedDate);
  for (var response of newResponses) {
    var rId = response.getId();
    var rUsername = response.getItemResponses()[0].getResponse().toString();
    if (rUsername.match(validResponse)) {
      sendDiscordMessage(rId, rUsername);
    } else {
      throw 'Invalid username received!';
    }
  }
  lock.releaseLock();
}

function sendDiscordMessage(id, username) {
  var request = {
    "method": "post",
    "headers": {
      "Content-Type": "application/json"
    },
    "payload": JSON.stringify({
      "content": "formverif verify " + id + "#" + username
    })
  };
  UrlFetchApp.fetch(POST_URL, request);
}
